#include "Cell.h"
#include <string>
#include "aditionalStringActions.h"
using std::string;
using std::cerr;
using std::endl;

/*
Inits a cell.
Input: dna_sequence, glucose receptor gene
Output: none.
*/
void Cell::init(const string dna_sequence, const Gene  glucose_receptor_gene)
{
	string dna_strand = dna_sequence;
	if (this->_glocuse_receptor_gene.is_on_complementary_dna_strand()) //If the strand that
	{
		replaceString(dna_strand, A_NUCLEOTIDE, T_NUCLEOTIDE);
		replaceString(dna_strand, G_NUCLEOTIDE, C_NUCLEOTIDE);
		replaceString(dna_strand, C_NUCLEOTIDE, G_NUCLEOTIDE);
		replaceString(dna_strand, T_NUCLEOTIDE, A_NUCLEOTIDE);
	}
		this->_nucleus.init(dna_sequence);
	
	this->_mitochondrion.init();
	this->_atp_units = 0;
	this->_glocuse_receptor_gene = glucose_receptor_gene;
}
/*
Try to charge the cell with energy.
Input: none.
Output: if the cell has charged.
*/
bool Cell::get_ATP()
{
	bool hasCharged = false;
	Protein* protein = NULL; //Protein from the rebosome.
	string transcript = this->_nucleus.get_RNA_transcript(this->_glocuse_receptor_gene);

	std::cout << "The original strand:" << std::endl;
	std::cout << this->_nucleus.get_dna_strand()<< std::endl;

	std::cout << "The complementary strand:" << std::endl;
	std::cout << this->_nucleus.get_complementary_dna_strand() << std::endl;

	std::cout << "The rna:" << std::endl;
	std::cout << transcript << std::endl;
	bool produceAtp = false; //If the mitochondrion has successed to create ATP
	protein = this->_ribosome.create_protein(transcript);

	if (protein) //If the rebosome has succeesed to cretae a protein.
	{
		this->_mitochondrion.insert_glocuse_receptor(*protein);
		this->_mitochondrion.set_glocuse_level(MIN_GLUCOSE_UNIT_FOR_PRODUCE_ATP);
		produceAtp = this->_mitochondrion.produceATP(this->_mitochondrion.get_glucose_level());
		if (produceAtp) //If the mitochondrion has successed to create ATP
		{
			this->_atp_units = FULL_ATP;
			hasCharged = true;
		}
	}
	else
	{
		cerr << "Wow, you'r in a trouble, the ribosome did not success to create a protein..." << endl;
		_exit(1);
	}



	return hasCharged;
}
