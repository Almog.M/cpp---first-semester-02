#include "Mitochondrion.h"
#include <string>

/*
Inits a mitochondrion object.
Input: none.
Output: none.
*/
void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}
/*
Getting a protein and check if this protein can be optional to produce glucose if yes turn on the 'glocuse' flag.
Input: a protein.
Output: none.
*/
void Mitochondrion::insert_glocuse_receptor(const Protein& protein)
{
	unsigned int proteinLen = protein.get_len();
	this->_has_glocuse_receptor = false;

	//I've used here in operators because this is useful in this case. 
	if (proteinLen == OPTIONAL_PROTEIN_TO_GLUCOSE_LEN)
	{
		if (protein[0] == ALANINE && protein[1] == LEUCINE && protein[2] == GLYCINE && protein[3] == HISTIDINE)
		{
			if (protein[4] == LEUCINE && protein[5] == PHENYLALANINE && protein[6] == AMINO_CHAIN_END)
			{
				this->_has_glocuse_receptor = true;
			}
		}
	}
}
/*
Getter for the glucose level in the glucose level in the mitochondrion.
Input: none.
Output: the glucose level.
*/
unsigned int Mitochondrion::get_glucose_level() const
{
	return this->_glocuse_level;
}
/*
Sets the glucose level of the mitochondrion.
Input: glucose units of the mitochondrion.
Output: none
*/
void Mitochondrion::set_glocuse_level(const unsigned int glocuse_units)
{
	this->_glocuse_level = (unsigned int)glocuse_units;
}
/*
Produce atp if it possible to do.
Input: glucose units of the mitochondrion.
Output: if we can to produce atp.
*/
bool Mitochondrion::produceATP(const int glocuse_unit)const
{
	bool toProduce = false;
	if (glocuse_unit >= MIN_GLUCOSE_UNIT_FOR_PRODUCE_ATP)
	{
		if (this->_has_glocuse_receptor)
		{
			toProduce = true;
		}
	}
	return toProduce;
}