#pragma once
#include <string>
#include "utility.h"
using std::string;

void replaceString(string& str, const char substitute, const char replaced);