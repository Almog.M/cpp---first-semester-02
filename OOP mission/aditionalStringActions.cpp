#include "aditionalStringActions.h"
#include <string>

void replaceString(string& str, const char substitute, const char replaced)
{
	unsigned int i = 0;
	unsigned int len = (unsigned int)str.length();

	for (i = 0; i < len; i++)
	{
		if (str[i] == replaced)
		{
			str[i] = substitute;
		}
	}
}