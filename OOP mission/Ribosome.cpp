#include "Ribosome.h"
#include <string>
#include "Protein.h"
#include "AminoAcid.h"
#include <iostream>

using std::string;
using std::cout;
using std::endl;

/*
Creates a protein by RNA transcript.
Input: RNA.
Output: the protein that suitable to the RNA.
*/
Protein* Ribosome::create_protein(const string& RNA_transcript)const
{
	Protein* protein = new Protein;
	protein->set_first(NULL);

	string changed_rna = (string)RNA_transcript;//The fake rna - the rna that changes always.
	string codon = "";
	bool stop = false; //If we will stop cut the rna or we have problem with a codon.
	AminoAcid acid = UNKNOWN; //As default...

	while (!stop)
	{
		if (changed_rna.length() >= CODON_LEN) //If we can to cut a codon from the rna.
		{
			codon = changed_rna.substr(0, CODON_LEN);
			/*cout << changed_rna << endl;
			cout << codon << endl;*/
			acid = get_amino_acid(codon);
			/*cout << acid << endl;*/
			changed_rna = changed_rna.substr(CODON_LEN);
			if (acid == UNKNOWN)
			{
				protein->clear();
				protein = nullptr;
				stop = true;
			}
			else
			{
				protein->add(acid);
			}
		}
		else
		{
			stop = true;
		}
	}

	return protein;
}