#pragma once
#include <string>
#include "Protein.h"
#include "utility.h"
using std::string;


class Ribosome
{
public:
	Protein* create_protein(const string& RNA_transcript) const;
};
