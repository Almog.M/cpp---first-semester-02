#include "Protein.h"
#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

/** initialize an empty list */
void LinkedList::init()
{
	this->_first = nullptr;
}

// getters
AminoAcidNode* LinkedList::get_first() const
{
	return this->_first;
}


// setters
void LinkedList::set_first(AminoAcidNode *first)
{
	this->_first = first;
}

/** adds an amino acid to the end of the list **/
void LinkedList::add(const AminoAcid amino_acid_to_add)
{
	// create a new node to store the amino acid
	AminoAcidNode *new_node = new AminoAcidNode;
	new_node->init(amino_acid_to_add);

	if (this->_first == nullptr) // list is empty
	{
		this->_first = new_node; // the new node is the first node
	}
	else        // list has at least one element
	{
		AminoAcidNode *curr = this->_first;      // start from first
		while (curr->get_next() != nullptr) // advance to the last element
		{
			curr = curr->get_next();
		}
		curr->set_next(new_node); // add the new node at the end of the list
	}
}

/** clears the list's memory */
void LinkedList::clear()
{
	AminoAcidNode* temp;
	while (this->_first != nullptr) // as long as the list is not empty
	{
		temp = this->_first;    // temp points to the first element
		this->_first = this->_first->get_next();   // the list now starts from the 2nd element
		delete temp;    // deletes the previous first element
	}
}

/* initialize a new Node object that stores the given data */
void AminoAcidNode::init(const AminoAcid amino_acid)
{
	this->_data = amino_acid;
	this->_next = nullptr;
}

// getters
AminoAcidNode* AminoAcidNode::get_next() const
{
	return this->_next;
}

AminoAcid AminoAcidNode::get_data() const
{
	return this->_data;
}

// setters
void AminoAcidNode::set_next(AminoAcidNode *next)
{
	this->_next = next;
}

void AminoAcidNode::set_data(const AminoAcid amino_acid)
{
	this->_data = amino_acid;
}

/*
Calcs the len of a protein.
Input: none.
Output: the len
*/
unsigned int LinkedList::get_len()const
{
	unsigned int len = 0;
	AminoAcidNode* node = this->_first;
	while (node)
	{
		len++;
		node = node->get_next();
	}
	return len;
} 
/*
Operator for finding a value in the protein, operator [].
Input: index of amino acid.
Output: the amino acid that in this location.
*/
AminoAcid LinkedList::operator [] (int index)const
{
	AminoAcidNode* node = this->_first;
	int counter = 0;
	AminoAcid acid = UNKNOWN;

	if ((unsigned int)index < this->get_len() || index < 0)
	{
		for(counter = 0; counter < index; counter++)
		{
			node = node->get_next();
		}
	}
	else
	{
		cerr << "The index out of range or negative..." << endl;
		_exit(1);
	}


	acid = node->get_data();

	return acid;
}
