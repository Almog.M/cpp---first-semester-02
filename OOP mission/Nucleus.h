#pragma once
#include <string>
#include "utility.h"
using std::string;

//Constants:
#define A_NUCLEOTIDE 'A'
#define T_NUCLEOTIDE 'T'
#define C_NUCLEOTIDE 'C'
#define G_NUCLEOTIDE 'G'
#define U_NUCLEOTIDE 'U'

#define A_NUCLEOTIDE_AS_SMALL 'a'
#define T_NUCLEOTIDE_AS_SMALL 't'
#define C_NUCLEOTIDE_AS_SMALL 'c'
#define G_NUCLEOTIDE_AS_SMALL 'g'





class Gene
{
	private:
		unsigned int _start;
		unsigned int _end;
		bool _on_complementary_dna_strand;

	public:
		//Constructive actions:
		void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);

		//Getters:
		unsigned int get_start() const;
		unsigned int get_end() const;
		bool is_on_complementary_dna_strand() const;

		//Setters
		void set_start(const unsigned int start);
		void set_end(const unsigned int end);
		void set_on_complementary_dna_strand(const bool on_complementary_dna_strand);
};

class Nucleus
{
	private:
		//fields
		string _DNA_strand;
		string _complementary_DNA_strand;

		//Methods:
		void initNucleotideForEachStrand(unsigned int index);
	public:
		
		//Getter:
		string get_dna_strand()const; //Gives the original strand
		string get_complementary_dna_strand()const; //Gives the complemenatary stran.

		//Constructive methods:
		void init(const string dna_sequence);
		
		//Other methods:
		string get_RNA_transcript(const Gene& gene)const;
		string get_reversed_DNA_strand() const;
		unsigned int get_num_of_codons_appearances(const string& codon) const;
};