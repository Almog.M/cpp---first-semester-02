#pragma once
#include <string>
#include "utility.h"
#include "Protein.h"

#define OPTIONAL_PROTEIN_TO_GLUCOSE_LEN 7//Len of the protein that optional to be used to produce glucose.
#define MIN_GLUCOSE_UNIT_FOR_PRODUCE_ATP 50

class Mitochondrion
{
private:
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;


public:
	//Constructive method:
	void init();

	//Others:
	void insert_glocuse_receptor(const Protein& protein);
	bool produceATP(const int glocuse_unit)const;

	//Getters:
	unsigned int get_glucose_level()const;
	//Setters:
	void set_glocuse_level(const unsigned int glocuse_unit);


};