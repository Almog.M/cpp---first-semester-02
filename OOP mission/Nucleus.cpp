#include "Nucleus.h"
#include <string>
#include <iostream>
#include "aditionalStringActions.h"

using std::cerr;
using std::endl;
using std::string;
using std::reverse;

//The complement for the Gene class:


//Constructive methods:
/*
Inits a Gene object.
Input: start index, end index, is the strand is elementary or not.
Output: none.
*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = (unsigned int)start;
	this->_end = (unsigned int)end;
	this->_on_complementary_dna_strand = (bool)on_complementary_dna_strand;
}


//getters:

/*
Gets start index from the gene obj.
Input: none.
Output: start index of the gene.
*/
unsigned int Gene::get_start() const
{
	return this->_start;
}

/*
Gets end index from the gene obj.
Input: none.
Output: end index of the gene.
*/
unsigned int Gene::get_end() const
{
	return this->_end;
}

/*
Gets the answer for - is the strand of the gene is complementary.
Input: none.
Output: the answer for the description below.
*/
bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}

//Setters:

/*
Sets the start value of the start index of the gene.
Input: the start value.
Output: none.
*/
void Gene::set_start(const unsigned int start)
{
	this->_start = (unsigned int)start;
}

/*
Sets the end value of the start index of the gene.
Input: the end value.
Output: none.
*/
void Gene::set_end(const unsigned int end)
{
	this->_end = (unsigned int)end;
}

/*
Sets if the strand of the gene is complementary.
Input: if its complementary.
Output: none.
*/
void Gene::set_on_complementary_dna_strand(const bool on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = (bool)on_complementary_dna_strand;
}














//Implementation of Nucleus class:

/*
Ints a Nucleotide in an index for each strand.
Input: index of a nucleotide to init.
Output: none.
*/
void Nucleus::initNucleotideForEachStrand(unsigned int index)
{
	if (this->_DNA_strand[index] == A_NUCLEOTIDE || this->_DNA_strand[index] == A_NUCLEOTIDE_AS_SMALL)
	{
		this->_DNA_strand[index] = A_NUCLEOTIDE; //For ensure that the dna string is in upper case. 
		this->_complementary_DNA_strand += T_NUCLEOTIDE;
	}
	else if (this->_DNA_strand[index] == T_NUCLEOTIDE || this->_DNA_strand[index] == T_NUCLEOTIDE_AS_SMALL)
	{
		this->_DNA_strand[index] = T_NUCLEOTIDE; //For ensure that the dna string is in upper case. 
		this->_complementary_DNA_strand += A_NUCLEOTIDE;
	}
	else if (this->_DNA_strand[index] == C_NUCLEOTIDE || this->_DNA_strand[index] == C_NUCLEOTIDE_AS_SMALL)
	{
		this->_DNA_strand[index] = C_NUCLEOTIDE; //For ensure that the dna string is in upper case. 
		this->_complementary_DNA_strand += G_NUCLEOTIDE;
	}
	else if (this->_DNA_strand[index] == G_NUCLEOTIDE || this->_DNA_strand[index] == G_NUCLEOTIDE_AS_SMALL)
	{
		this->_DNA_strand[index] = G_NUCLEOTIDE; //For ensure that the dna string is in upper case. 
		this->_complementary_DNA_strand += C_NUCLEOTIDE;
	}
	else //If the DNA sequence contains a letter that not a necleotid.
	{
		cerr << "The DNA sequence has to be with a,t,c,g or A,T,C,G only..." << std::endl;
		_exit(1);
	}
}

//Constructive methods:
/*
Inits a nucleus obj.
Input: dna sequence (regular, not complementary).
Output: none.
*/
void Nucleus::init(const string dna_sequence)
{
	unsigned int i = 0;
	unsigned int dna_len = (unsigned int)dna_sequence.length();

	this->_complementary_DNA_strand = "";
	this->_DNA_strand = (string)dna_sequence;
	
	for (i = 0; i < dna_len; i++)
	{
		initNucleotideForEachStrand(i);
	}
}
/*
Gives a RNA of a gene in the dna.
Input: a gene reference.
Output: the RNA of this gene.
*/
string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	string rna = "";
	unsigned int delta = gene.get_end() - gene.get_start(); //The delta between the gene indexes.
	
	if (gene.is_on_complementary_dna_strand()) //If the gene in the complementary strand.
	{
		rna = this->_complementary_DNA_strand.substr(gene.get_start(), delta + 1);
	}
	else
	{
		rna = this->_DNA_strand.substr(gene.get_start(), delta + 1);
	}

	replaceString(rna, U_NUCLEOTIDE, T_NUCLEOTIDE);

	return rna;
}
/*
Gives the reverse of the regular DNA strand.
Input: none.
Output: the reversed strand.
*/
string Nucleus::get_reversed_DNA_strand() const
{
	string reversed = "";
	reverse(this->_DNA_strand.begin(), this->_DNA_strand.end()); //Reverse
	reversed = this->_DNA_strand;
	reverse(this->_DNA_strand.begin(), this->_DNA_strand.end()); //Reverse again for do not change the original strand.
	return reversed;
}
/*
Gives the number of appearances that a codon appears in the regular DNA strand.
Input: reference of a codon.
Output: the number sum.
*/
unsigned int Nucleus::get_num_of_codons_appearances(const string& codon) const
{
	unsigned int times = 0; //Times of appearances.
	string temp = this->_DNA_strand;// A string that always shorts.
	bool is_find = true; //If we find the codon in the temp string.
	int index = 0; //Index of a new start of temp.
	int indexOfCodon = 0;
	int tempSize = (int)temp.length();
	unsigned int codonSize = (unsigned int)codon.length();

	while (is_find && index < tempSize)
	{
		indexOfCodon = (int)temp.find(codon);
		if (indexOfCodon != -1) //If the codon has founded in the temp string (:
		{
			times++;
			index = indexOfCodon + codonSize; //Index of the new temp like I've said...
			tempSize = (int)temp.length();
			if (index < tempSize) //If we want to create a new temp - a short one.
			{
				temp = temp.substr(index); //Creates the new temp index one after the appearance of the codon.
			}
		}
		else
		{
			is_find = false; //We do not find the codon in the temp ):
		}
	}

	return times;
}

string Nucleus::get_dna_strand()const
{
	return this->_DNA_strand;
}
string Nucleus::get_complementary_dna_strand()const
{
	return this->_complementary_DNA_strand;
}
